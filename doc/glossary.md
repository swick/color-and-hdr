---
SPDX-FileCopyrightText: 2020-2021 Collabora, Ltd.
SPDX-License-Identifier: MIT
---

Contents

[[_TOC_]]

[Front page](../README.md)


# Glossary

#### alpha

A value that denotes the portion of a pixel covered by the pixel's
color. When not 100%, the remainder of the pixel is transparent.
Sometimes used for translucency instead.

#### color channel

One of the dimensions in a color model. For example, RGB model has
channels R, G, and B. [Alpha](#alpha) channel is sometimes counted as a
color channel as well.

#### color value

A tuple of numbers, representing a color in some color system. Each
element in the tuple is a [color channel](#color-channel) value.

#### color volume

A three dimensional space of [color value](#color-value)s. This
includes both "color" (e.g. hue and saturation) and brightness
(luminance).

#### container color volume

This is the color volume addressable by a given signal container format
([pixel format](#pixel-format), encoding, defining color space). An
example is BT.2020/PQ as is standard for driving many consumer HDR
monitors.

As a special case, the container color volume with floating-point
[pixel format](#pixel-format)s is practically infinite, because
negative and greater than 1.0 values can be represented. The defining
color space and dynamic range are likely anchored to channel values 0.0
and 1.0, but due to extrapolation the representable color volume is not
limited by these. The target color volume needs to be specified
separately.

See also: [target color volume](#target-color-volume)

#### image

Conceptually a two-dimensional array of [pixel](#pixel)s. The pixels
may be stored in one or more [memory buffer](#memory-buffer)s. Has
width and height in pixels, a [pixel format](#pixel-format) and a
[modifier](#modifier) (implicit or explicit).

#### memory buffer

A piece of memory for storing (parts of) [pixel data](#pixel-data). Has
stride and size in bytes and at least one handle in some [API](#API).
Contains one or more [planes](#plane).

#### modifier

A description of how [pixel data](#pixel-data) is laid out in [memory
buffer](#memory-buffer)s.

#### nit

An absolute unit of luminance: cd/m², candelas per square meter.
Candela is a measure of luminous intensity, accounting for the
human eye sensitivity to light.

#### pixel

A picture element. Has a single [color value](#color-value) which is
one or more [color channel](#color-channel) values, e.g. R, G and B, or
Y, Cb and Cr. May also have an [alpha](#alpha) value as an additional
channel.

#### pixel data

Bytes or bits that represent some or all of the color/alpha channel
values of a [pixel](#pixel) or an [image](#image). The data for one
pixel may be spread over several [plane](#plane)s or [memory
buffer](#memory-buffer)s depending on [format](#pixel-format) and
[modifier](#modifier).

#### pixel format

A description of how [pixel data](#pixel-data) represents a pixel's
[color](#color-value) and [alpha](#alpha) values.

#### plane

In [image](#image) storage:

* A two-dimensional array of some or all of an [image](#image)'s
[color](#color-value) and [alpha](#alpha) values.

In [DRM](#DRM) [KMS](#KMS):

* A hardware composition element capable of holding one framebuffer
([image](#image)) and having position and stacking order on screen.

#### target color volume

The color volume intended to be usable by a given video or image
signal. That is, the signal has been produced targeting this color
volume in the mastering process. An example is using HDR static
metadata (SMPTE ST 2086) to describe the image content when the
container is BT.2020/PQ.

Traditional container formats and ICC v2-v4 profiles assume that
container color volume equals target color volume. This makes the most
use of the pixel storage. Using target color volume smaller than the
container color volume means that some code points will never be used,
which is wasteful for storage.

See also: [container color volume](#container-color-volume)

#### Wayland

A window system protocol.

#### X11

A window system protocol.


# Acronyms

#### API

application programming interface

#### AVI

auxiliary video information (infoframe)

#### CM

color management

#### CMM

color management module

#### CRTC

cathode-ray tube controller, nowadays a hardware block or an abstraction
that produces a timed stream of raw digital video data

#### DRM

direct rendering manager

#### EOFT

electro-optical transfer function

#### HDR

high dynamic range

#### KMS

kernel modesetting, display driver kernel-userspace API

#### LUT

look-up table

#### OETF

opto-electrical transfer function

#### SDR

standard dynamic range

#### VCGT

video card gamma table, a non-standard tag added into ICC profiles
originally by Apple
